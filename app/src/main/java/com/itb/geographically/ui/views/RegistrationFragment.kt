package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.view.animation.AnimationUtils.loadAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geographically.R
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.regex.Pattern

class RegistrationFragment : Fragment(R.layout.fragment_registration) {
    //ATTRIBUTES
    private lateinit var closingIcon: ImageView
    private lateinit var emailLayout: TextInputLayout
    private lateinit var emailInput: EditText
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var passwordInput: EditText
    private lateinit var repeatPasswordLayout: TextInputLayout
    private lateinit var repeatPasswordInput: EditText
    private lateinit var signUpButton: Button

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        view.startAnimation(loadAnimation(requireContext(), R.anim.fragment_enter_animation))

        //IDs
        closingIcon = view.findViewById(R.id.close_button)
        emailLayout = view.findViewById(R.id.text_input_layout1)
        emailInput = view.findViewById(R.id.email_sign_up_input)
        passwordLayout = view.findViewById(R.id.text_input_layout2)
        passwordInput = view.findViewById(R.id.password_sign_up_input)
        repeatPasswordLayout = view.findViewById(R.id.text_input_layout3)
        repeatPasswordInput = view.findViewById(R.id.repeat_password_sign_up_input)
        signUpButton = view.findViewById(R.id.registration_button)

        //ON CLICK
        //Closing Icon
        closingIcon.setOnClickListener {
            exitFragment()
        }

        //Password Input (If not used, the error icon stops the user from using the eye icon)
        passwordInput.setOnClickListener {
            passwordLayout.error = null
        }
        passwordInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                passwordLayout.error = null
        }

        //Repeat Password Input (If not used, the error icon stops the user from using the eye icon)
        repeatPasswordInput.setOnClickListener {
            repeatPasswordLayout.error = null
        }
        repeatPasswordInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                repeatPasswordLayout.error = null
        }

        //Sign Up Button
        signUpButton.setOnClickListener {
            validateRegistration()
        }
    }

    //METHODS
    /**
     * This method is used to exit the fragment.
     */
    private fun exitFragment() {
        //EXIT ANIMATION
        view?.startAnimation(loadAnimation(requireContext(), R.anim.fragment_exit_animation))

        //NAVIGATION
        findNavController().navigate(R.id.action_registration_to_login)
    }

    /**
     * This method is used to validate the registration inputs.
     */
    @SuppressLint("SimpleDateFormat")
    private fun validateRegistration() {
        //ERROR CONTROL
        //Invalid Email
        if (!Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches())
            emailLayout.error = "Invalid email address"
        //No Email
        if (emailInput.text.toString() == "")
            emailLayout.error = "Email is required"
        //Valid Email (Used when previously no / invalid email and now no password)
        if (Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches())
            emailLayout.error = null
        //Invalid Password
        if (!Pattern.compile("[a-zA-Z0-9!@#$]{8,24}").matcher(passwordInput.text.toString()).matches())
            passwordLayout.error = "Invalid password"
        //No Password
        if (passwordInput.text.toString() == "")
            passwordLayout.error = "Password is required"
        //Valid Password (Used when no / invalid password previously and now no email or invalid)
        if (Pattern.compile("[a-zA-Z0-9!@#$]{8,24}").matcher(passwordInput.text.toString()).matches())
            passwordLayout.error = null
        //Invalid Repeat Password
        if (repeatPasswordInput.text.toString() != passwordInput.text.toString())
            repeatPasswordLayout.error = "Passwords are NOT matching"
        //No Repeat Password
        if (repeatPasswordInput.text.toString() == "")
            repeatPasswordLayout.error = "Password is required"
        //Valid Repeat Password
        if (repeatPasswordInput.text.toString() == passwordInput.text.toString() && (passwordInput.text.toString() != ""))
            repeatPasswordLayout.error = null
        //Valid Email + Password + Repeat Password
        if (Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches()
            && Pattern.compile("[a-zA-Z0-9!@#$]{8,24}").matcher(passwordInput.text.toString()).matches()
            && repeatPasswordInput.text.toString() == passwordInput.text.toString()) {
            emailLayout.error = null
            passwordLayout.error = null
            repeatPasswordLayout.error = null

            //FIREBASE REGISTRATION
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(emailInput.text.toString(), passwordInput.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        //USER DATA UPDATE (ViewModel)
                        viewModel.email = it.result?.user?.email
                        viewModel.password = passwordInput.text.toString()

                        //FIRESTORE INSERT
                        Firebase.firestore.collection("users").document(it.result?.user?.email!!).set(hashMapOf("mapStyle" to "Standard"))

                        //NAVIGATION
                        exitFragment()
                    }
                    else
                        Toast.makeText(requireContext(), "This account already exists", Toast.LENGTH_SHORT).show()
                }
        }
    }
}
