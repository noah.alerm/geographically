package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.itb.geographically.R
import com.itb.geographically.ui.adapters.TagAdapter
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.DialogCustomizer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat

class DetailFragment : Fragment(R.layout.fragment_detail) {
    //ATTRIBUTES
    private lateinit var loadingPanel: RelativeLayout
    private lateinit var closingButton: ImageView
    private lateinit var editMarkerIcon: ImageView
    private lateinit var image: ImageView
    private lateinit var favoritesIcon: ImageView
    private lateinit var name: TextView
    private lateinit var type: TextView
    private lateinit var description: TextView
    private lateinit var creationDate: TextView
    private lateinit var addTagIcon: ImageView
    private lateinit var tagRecyclerView: RecyclerView
    private lateinit var goToMapIcon: ImageView
    private lateinit var arrow: ImageView
    private lateinit var goToMapIconTip: TextView

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        if (!viewModel.stopAnimation)
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))
        else
            viewModel.stopAnimation = false

        //IDs
        loadingPanel = view.findViewById(R.id.loading_panel)
        closingButton = view.findViewById(R.id.close_button)
        editMarkerIcon = view.findViewById(R.id.edit_marker_icon)
        image = view.findViewById(R.id.image)
        favoritesIcon = view.findViewById(R.id.favorites_icon)
        name = view.findViewById(R.id.name)
        type = view.findViewById(R.id.type)
        description = view.findViewById(R.id.description)
        creationDate = view.findViewById(R.id.creation_date)
        addTagIcon = view.findViewById(R.id.add_tag_icon)
        tagRecyclerView = view.findViewById(R.id.tags_recycler_view)
        goToMapIcon = view.findViewById(R.id.go_to_map_icon)
        arrow = view.findViewById(R.id.arrow)
        goToMapIconTip = view.findViewById(R.id.map_icon_tip)

        //LAYOUT UPDATE (Info)
        if (viewModel.firstTimeInDetail) {
            viewModel.firstTimeInDetail = false
            arrow.visibility = View.VISIBLE
            goToMapIconTip.visibility = View.VISIBLE
        }

        //MARKER
        val marker = viewModel.markers.filter { it.id == arguments!!.getLong("id") }[0]

        //RECYCLER VIEW SET UP
        tagRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        tagRecyclerView.adapter = TagAdapter(marker.tags, marker)

        //DATA SET UP
        if (marker.image != null)
            image.setImageBitmap(marker.image)
        else {
            //LOADING PANEL
            loadingPanel.visibility = View.VISIBLE

            var imageUri: Uri? = null

            FirebaseStorage.getInstance().getReferenceFromUrl("gs://geographically-341418.appspot.com/")
                .child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/" + marker.id)
                .downloadUrl.addOnSuccessListener {
                    imageUri = it
                    Log.d("URI", imageUri.toString())
                }

            Handler(Looper.getMainLooper()).postDelayed({
                if (imageUri != null) {
                    Glide.with(name.context).asBitmap().load(imageUri).into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            image.setImageBitmap(resource)
                            marker.image = resource
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                    })
                }
                //LOADING PANEL
                loadingPanel.visibility = View.GONE
            }, 1000)
        }

        if (marker.isFavorite)
            favoritesIcon.setImageResource(R.drawable.favorites_icon)
        else
            favoritesIcon.setImageResource(R.drawable.no_favorites_icon)

        //Name length control
        if (marker.name.length > 23)
            name.text = "${marker.name.subSequence(0, 20)}..."
        else
            name.text = marker.name

        type.text = marker.type

        if (marker.description == "")
            description.text = "-"
        else
            description.text = marker.description

        creationDate.text = SimpleDateFormat("HH:mm  dd/MM/yyyy").format(marker.creationDate)

        //ON CLICK
        //Closing Button
        closingButton.setOnClickListener {
            //EXIT ANIMATION
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))
            viewModel.stopAnimation = true

            //NAVIGATION
            when (viewModel.currentFragment) {
                "List" -> findNavController().navigate(R.id.action_detail_to_markerList, bundleOf("isFavorites" to false))
                "Favorites" -> findNavController().navigate(R.id.action_detail_to_markerList, bundleOf("isFavorites" to true))
                "Map" -> findNavController().navigate(R.id.action_detail_to_map)
            }
        }

        //Edit Marker Icon
        editMarkerIcon.setOnClickListener {
            //NAVIGATION
            findNavController().navigate(R.id.action_detail_to_editMarker, bundleOf("id" to marker.id))
        }

        //Favorites Icon
        favoritesIcon.setOnClickListener {
            //ICON ANIMATION
            YoYo.with(Techniques.Pulse).duration(400).playOn(favoritesIcon)

            marker.isFavorite = !marker.isFavorite

            //FIRESTORE UPDATE
            Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                .collection("markers").document(marker.id.toString()).update(mapOf("isFavorite" to marker.isFavorite))

            //ICON UPDATE
            if (marker.isFavorite)
                favoritesIcon.setImageResource(R.drawable.favorites_icon)
            else
                favoritesIcon.setImageResource(R.drawable.no_favorites_icon)
        }

        //Add Tag Icon
        addTagIcon.setOnClickListener {
            //ICON ANIMATION
            YoYo.with(Techniques.RotateIn).duration(400).playOn(addTagIcon)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_add_tag_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Tag Addition", requireContext()))
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("ADD") { _, _ ->
                //ATTRIBUTES
                val tagNameInput: EditText = viewDialog.findViewById(R.id.tag_name_input)

                //ADDITION
                if (tagNameInput.text.toString() != "") {
                    //LOCAL UPDATE
                    marker.tags.add(tagNameInput.text.toString())
                    tagRecyclerView.adapter = TagAdapter(marker.tags, marker)

                    //FIRESTORE UPDATE
                    Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                        .collection("markers").document(marker.id.toString()).update(mapOf("tags" to marker.tags.toList()))
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Go To Map Icon
        goToMapIcon.setOnClickListener {
            //EXIT ANIMATION + MARKER ANIMATION SET UP
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))
            viewModel.stopAnimation = true
            viewModel.markerAnimationCoords = marker.coords

            //NAVIGATION
            findNavController().navigate(R.id.action_detail_to_map)
        }
    }

    //ON STOP
    override fun onStop() {
        super.onStop()
        //VIEW MODEL UPDATE (If not done here, the data is reset and preserved when it shouldn't)
        viewModel.bitmap = null
        viewModel.bitmapChanged = false
    }
}
