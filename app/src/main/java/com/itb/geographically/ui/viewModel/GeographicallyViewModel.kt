package com.itb.geographically.ui.viewModel

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.itb.geographically.data.models.MarkerGeo
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class GeographicallyViewModel : ViewModel() {
    //ATTRIBUTES
    //User data
    var email: String? = ""
    var password: String = ""

    //Map style
    var mapThemes = listOf("Aubergine", "Dark", "Night", "Retro", "Silver", "Standard")
    var currentMapTheme = ""

    //Markers
    var typesOfLocation = listOf("Beach", "Cityscape", "Hiking trail", "Landscape", "Park",
        "Promenade", "Restaurant", "Other")
    var markers = mutableListOf<MarkerGeo>()
    var liveMarkers = MutableLiveData<List<MarkerGeo>>()
    var counter: Long = 0 //Used to obtain a new ID for a marker
    var markerAnimationCoords: LatLng? = null

    //Booleans
    var stopAnimation: Boolean = false
    var firstTimeAddFromList: Boolean = true
    var firstTimeInDetail: Boolean = true
    var firstTimeInList: Boolean = true

    //App navigation
    var currentFragment: String? = ""
    var currentPosition: LatLng? = null

    //Orientation Change Persistence
    var bitmap: Bitmap? = null
    var bitmapChanged = false
    var newUsername = ""
    var editMode = false

    //METHODS
    /**
     * This method is used to obtain the the current user's markers from the Firestore database.
     */
    fun getMarkersFromFirestore() {
        //MARKER LIST RESET
        markers.clear()

        //FIRESTORE MARKER GETTER
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
            .collection("markers").get().addOnSuccessListener { query ->
                //Document Loop
                for (document in query) {
                    markers.add(
                        MarkerGeo(document.id.toLong(), document.getString("name")!!,
                        LatLng(document.getGeoPoint("coords")!!.latitude, document.getGeoPoint("coords")!!.longitude),
                        document.getString("type")!!, document.getString("description")!!, null,
                        document.getDate("creationDate")!!, document.get("tags") as MutableList<String>,
                        document.getBoolean("isFavorite")!!)
                    )
                }

                //ID COUNTER UPDATE
                counter = if (markers.size != 0)
                    markers.last().id+1
                else
                    0

                liveMarkers.postValue(markers)
            }
    }

    /**
     * This method is used to insert a new marker into the Firestore database.
     * @param marker Marker to be inserted
     */
    fun insertMarkerIntoFirestore(marker: MarkerGeo) {
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
            .collection("markers").document(marker.id.toString()).set(hashMapOf("name" to marker.name,
                "coords" to GeoPoint(marker.coords.latitude, marker.coords.longitude), "type" to marker.type, "description" to marker.description,
                "creationDate" to marker.creationDate, "tags" to marker.tags.toList(), "isFavorite" to marker.isFavorite))
    }
}
