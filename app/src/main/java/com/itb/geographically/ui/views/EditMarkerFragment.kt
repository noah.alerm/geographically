package com.itb.geographically.ui.views

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.CameraAndGalleryManagement
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage

class EditMarkerFragment : Fragment(R.layout.fragment_edit_marker){
    //ATTRIBUTES
    private lateinit var image: ImageView
    private lateinit var rotationIcon: ImageView
    private lateinit var deleteImageIcon: ImageView
    private lateinit var cameraIcon: ImageView
    private lateinit var galleryIcon: ImageView
    private lateinit var nameLayout: TextInputLayout
    private lateinit var nameInput: EditText
    private lateinit var typeLayout: TextInputLayout
    private lateinit var typeInput: AutoCompleteTextView
    private lateinit var descriptionLayout: TextInputLayout
    private lateinit var descriptionInput: EditText
    private lateinit var saveChangesButton: Button
    private lateinit var cancelChangesButton: AppCompatButton

    //Bitmap
    private var bitmap: Bitmap? = null

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        if (!viewModel.stopAnimation)
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))
        else
            viewModel.stopAnimation = false

        //IDs
        image = view.findViewById(R.id.image)
        rotationIcon = view.findViewById(R.id.rotation_icon)
        deleteImageIcon = view.findViewById(R.id.delete_image_button)
        cameraIcon = view.findViewById(R.id.camera_icon)
        galleryIcon = view.findViewById(R.id.gallery_icon)
        nameLayout = view.findViewById(R.id.text_input_layout1)
        nameInput = view.findViewById(R.id.name_input)
        typeLayout = view.findViewById(R.id.text_input_layout2)
        typeInput = view.findViewById(R.id.type_input)
        descriptionLayout = view.findViewById(R.id.text_input_layout3)
        descriptionInput = view.findViewById(R.id.description_input)
        saveChangesButton = view.findViewById(R.id.save_button)
        cancelChangesButton = view.findViewById(R.id.cancel_button)

        //MARKER
        val marker = viewModel.markers.filter { it.id == arguments!!.getLong("id") }[0]

        //DATA SET UP
        if (marker.image == null)
            image.setImageResource(R.drawable.default_image)
        else {
            bitmap = marker.image!!
            image.setImageBitmap(marker.image)
        }

        nameInput.setText(marker.name)
        typeInput.setText(marker.type)
        descriptionInput.setText(marker.description)

        //TYPE INPUT SET UP
        typeInput.setAdapter(ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, viewModel.typesOfLocation))

        //ON CLICK
        //Rotation Icon
        rotationIcon.setOnClickListener {
            if (bitmap != null) {
                //ROTATION
                val matrix = Matrix()
                matrix.postRotate(90F)
                bitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap!!.width, bitmap!!.height, matrix, true)

                //IMAGE UPDATE
                image.setImageBitmap(bitmap)
                viewModel.bitmapChanged = true
            }
        }

        //Delete Image Icon
        deleteImageIcon.setOnClickListener {
            bitmap = null
            viewModel.bitmap = null
            viewModel.bitmapChanged = true
            image.setImageResource(R.drawable.default_image)
        }

        //Camera Icon
        cameraIcon.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access camera", Toast.LENGTH_SHORT).show()
            }
        }

        //Gallery Icon
        galleryIcon.setOnClickListener {
            val getPictureIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            try {
                startActivityForResult(getPictureIntent, 2)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access gallery", Toast.LENGTH_SHORT).show()
            }
        }

        //Save Changes Button
        saveChangesButton.setOnClickListener {
            //MARKER UPDATE
            validateInputs(marker)

            //NAVIGATION
            exitFragment(marker)
        }

        //Cancel Changes Button
        cancelChangesButton.setOnClickListener {
            //NAVIGATION
            exitFragment(marker)
        }
    }

    //METHODS
    /**
     * This method is used to to exit this fragment.
     * @param marker Used to get the ID of the marker that must be shown in the next fragment
     */
    private fun exitFragment(marker: MarkerGeo) {
        //EXIT ANIMATION
        view?.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))

        //VIEW MODEL UPDATE
        viewModel.bitmap = null

        //NAVIGATION
        viewModel.stopAnimation
        findNavController().navigate(R.id.action_editMarker_to_detail, bundleOf("id" to marker.id))
    }

    /**
     * This method is used to validate all inputs.
     * @param marker Marker to update
     */
    private fun validateInputs(marker: MarkerGeo) {
        //ERROR CONTROL
        //No Name
        if (nameInput.text.toString() == "")
            nameLayout.error = "Name is required"
        //Name
        if (nameInput.text.toString() != "")
            nameLayout.error = null
        //No Type
        if (typeInput.text.toString() == "")
            typeLayout.error = "Type is required"
        //Type
        if (typeInput.text.toString() != "")
            typeLayout.error = null
        //Name + Type
        if (nameInput.text.toString() != "" && typeInput.text.toString() != "") {
            nameLayout.error = null
            typeLayout.error = null

            //MARKER UPDATE
            marker.name = nameInput.text.toString()
            marker.type = typeInput.text.toString()
            marker.description = descriptionInput.text.toString()
            marker.image = bitmap

            //FIRESTORE UPDATE
            Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                .collection("markers").document(marker.id.toString()).update(mapOf("name" to marker.name,
                    "type" to marker.type, "description" to marker.description))

            //FIREBASE STORAGE for images
            if (marker.image != null)
                CameraAndGalleryManagement.addImageToStorage(marker)
            else
                FirebaseStorage.getInstance().reference.child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/" + marker.id).delete()
        }
    }

    //ON ACTIVITY RESULT
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        bitmap = CameraAndGalleryManagement.getBitmapFromRequest(requestCode, resultCode, data, activity!!, image, false)

        //VIEW MODEL UPDATE
        viewModel.bitmapChanged = true
    }

    //ORIENTATION CHANGE CONTROL

    //ON PAUSE
    override fun onPause() {
        super.onPause()
        //IMAGE PERSISTENCE
        viewModel.bitmap = bitmap
    }

    //ON VIEW STATE RESTORED
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        //IMAGE PERSISTENCE
        if (viewModel.bitmapChanged) {
            if (viewModel.bitmap != null) {
                bitmap = viewModel.bitmap
                image.setImageBitmap(bitmap)
            }
            else
                image.setImageResource(R.drawable.default_image)
        }
    }
}
