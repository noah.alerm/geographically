package com.itb.geographically.ui.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.DialogCustomizer
import com.itb.geographically.utils.NavigationDrawerOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MapFragment : Fragment(R.layout.fragment_map), OnMapReadyCallback, GoogleMap.InfoWindowAdapter {
    //ATTRIBUTES
    private lateinit var loadingPanel: RelativeLayout
    private lateinit var map: GoogleMap
    private lateinit var menuIcon: ImageView
    private lateinit var styleIcon: ImageView
    private lateinit var infoIcon: ImageView
    private lateinit var addButton: FloatingActionButton

    //Navigation Drawer
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON CREATE VIEW
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView =  inflater.inflate(R.layout.fragment_map, container, false)
        createMap()

        //LOADING PANEL ID
        loadingPanel = rootView.findViewById(R.id.loading_panel)

        return rootView
    }

    //ON MAP READY
    override fun onMapReady(googleMap: GoogleMap) {
        //LOADING PANEL
        loadingPanel.visibility = View.VISIBLE

        //MAP INITIALIZATION
        map = googleMap
        viewModel.liveMarkers.observe(viewLifecycleOwner, { createMarkers(it) })
        enableLocation()

        //MARKER INFO
        map.setInfoWindowAdapter(this)

        //MAP STYLE
        if (viewModel.currentMapTheme == "") {
            Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                .get().addOnSuccessListener {
                    viewModel.currentMapTheme = it.getString("mapStyle")!!
                }

            Handler(Looper.getMainLooper()).postDelayed({
                setUpMapStyle()
            }, 1000)
        }
        else
            setUpMapStyle()
    }

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //CURRENT FRAGMENT (Used when the user comes back from the User Fragment)
        viewModel.currentFragment = "Map"

        //ENTER ANIMATION
        if (!viewModel.stopAnimation)
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))
        else
            viewModel.stopAnimation = false

        //IDs
        menuIcon = view.findViewById(R.id.menu_icon)
        styleIcon = view.findViewById(R.id.style_icon)
        infoIcon = view.findViewById(R.id.info_icon)
        addButton = view.findViewById(R.id.add_button)
        drawerLayout = activity!!.findViewById(R.id.drawerLayout)
        navigationView = activity!!.findViewById(R.id.navigationView)

        //ON CLICK
        //Menu Icon
        menuIcon.setOnClickListener {
            NavigationDrawerOptions.manageNavigationOptions(drawerLayout, navigationView, activity!!, viewModel)
        }

        //Style Icon
        styleIcon.setOnClickListener {
            //ICON ANIMATION
            YoYo.with(Techniques.Tada).duration(400).playOn(styleIcon)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_map_style_dialog, null)

            //Attributes
            val themeInput: AutoCompleteTextView = viewDialog.findViewById(R.id.theme_input)

            //Type Input
            themeInput.setAdapter(ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, viewModel.mapThemes))

            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Map Theme", requireContext()))
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("APPLY") { _, _ ->
                //STYLE SETTER
                when (themeInput.text.toString()) {
                    "Standard" -> {
                        viewModel.currentMapTheme = "Standard"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.standard_style))
                    }
                    "Aubergine" -> {
                        viewModel.currentMapTheme = "Aubergine"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.aubergine_style))
                    }
                    "Dark" -> {
                        viewModel.currentMapTheme = "Dark"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.dark_style))
                    }
                    "Night" -> {
                        viewModel.currentMapTheme = "Night"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.night_style))
                    }
                    "Retro" -> {
                        viewModel.currentMapTheme = "Retro"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.retro_style))
                    }
                    "Silver" -> {
                        viewModel.currentMapTheme = "Silver"
                        map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.silver_style))
                    }
                }

                //FIRESTORE UPDATE
                Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                    .update(mapOf("mapStyle" to themeInput.text.toString()))
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Info Icon
        infoIcon.setOnClickListener {
            //ICON ANIMATION
            YoYo.with(Techniques.RotateIn).duration(400).playOn(infoIcon)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_map_info_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Map Info", requireContext()))
            builder.setPositiveButton("OK", null)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Handler used because the map takes some time to load.
        Handler(Looper.getMainLooper()).postDelayed({
            //MARKER ANIMATION FROM DETAIL
            if (viewModel.markerAnimationCoords != null) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(viewModel.markerAnimationCoords!!, 18f), 2500, null)
                viewModel.markerAnimationCoords = null
            }

            //Add Button
            addButton.setOnClickListener {
                //NAVIGATION
                if (!map.isMyLocationEnabled) {
                    Toast.makeText(requireContext(), "Make sure you have location permissions enabled", Toast.LENGTH_SHORT).show()

                    enableLocation()
                }
                else {
                    if (map.myLocation != null) {
                        val action = MapFragmentDirections.actionMapToAddMarker(map.myLocation.latitude.toFloat(), map.myLocation.longitude.toFloat())
                        findNavController().navigate(action)
                    }
                }
            }

            //Map
            map.setOnMapLongClickListener {
                //NAVIGATION
                val action = MapFragmentDirections.actionMapToAddMarker(it.latitude.toFloat(), it.longitude.toFloat())
                findNavController().navigate(action)
            }

            //Info Window
            map.setOnInfoWindowClickListener {
                //NAVIGATION
                val id: Long = it.tag as Long
                findNavController().navigate(R.id.action_map_to_detail, bundleOf("id" to id))
            }
        }, 700)
    }


    //MAP METHODS
    /**
     * This method is used to create the map.
     */
    private fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    /**
     * This method is used to create all markers.
     */
    private fun createMarkers(markers: List<MarkerGeo>){
        //MARKER LOOP
        for (marker in markers) {
            val myMarker = MarkerOptions().position(marker.coords).title(marker.name).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon))
            val m = map.addMarker(myMarker)
            m!!.tag = marker.id
        }
    }

    //LOCATION METHODS
    /**
     * This method is used to check if the location permission is granted.
     */
    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * This method is used to enable the location.
     */
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    /**
     * This method is used to request the location permission.
     */
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Go to Settings and turn locations on", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 100)
        }
    }

    /**
     * This method is used to request the permission's result.
     */
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            100 -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accept the location permissions",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    //ON RESUME
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accept the location permissions", Toast.LENGTH_SHORT).show()
        }
    }

    //ON STOP
    override fun onStop() {
        super.onStop()
        if (map.isMyLocationEnabled) {
            if (map.myLocation != null) {
                //VIEW MODEL UPDATE
                viewModel.currentPosition = LatLng(map.myLocation.latitude, map.myLocation.longitude)
                viewModel.bitmap = null //If not done here, the bitmap is reset and the image is preserved when it shouldn't
            }
        }
    }

    //INFO WINDOW METHODS
    /**
     * This method is used to set up the info window layout.
     * @param view The Info Window's layout
     * @param marker The marker (Marker, not MarkerGeo)
     */
    @SuppressLint("SetTextI18n")
    private fun setUpInfoWindow(view: View?, marker: Marker) {
        //ATTRIBUTES
        val name: TextView = view!!.findViewById(R.id.marker_name)
        val type: TextView = view.findViewById(R.id.marker_type)

        //MARKER
        val markerGeo = viewModel.markers.filter { it.id == marker.tag }[0]

        //SET UP
        if (markerGeo.name.length > 15)
            name.text = "${markerGeo.name.subSequence(0, 12)}..."
        else
            name.text = markerGeo.name

        if (markerGeo.type.length > 15)
            type.text = "${markerGeo.type.subSequence(0, 12)}..."
        else
            type.text = markerGeo.type
    }

    @SuppressLint("InflateParams")
    override fun getInfoContents(marker: Marker): View? {
        //LAYOUT
        val view = layoutInflater.inflate(R.layout.custom_info_window, null)

        setUpInfoWindow(view, marker)

        return view
    }

    @SuppressLint("InflateParams")
    override fun getInfoWindow(marker: Marker): View? {
        //LAYOUT
        val view = layoutInflater.inflate(R.layout.custom_info_window, null)

        setUpInfoWindow(view, marker)

        return view
    }

    //METHODS
    /**
     * This method is used to set up the map's style.
     */
    private fun setUpMapStyle() {
        when (viewModel.currentMapTheme) {
            "Standard" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.standard_style))
            "Aubergine" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.aubergine_style))
            "Dark" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.dark_style))
            "Night" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.night_style))
            "Retro" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.retro_style))
            "Silver" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.silver_style))
        }

        //LOADING PANEL
        loadingPanel.visibility = View.GONE
    }
}
