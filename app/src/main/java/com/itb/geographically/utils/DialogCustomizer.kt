package com.itb.geographically.utils

import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import com.itb.geographically.R

object DialogCustomizer {
    //METHODS
    /**
     * This method is used to obtain a customized dialog title.
     * @param titleText Dialog Title
     */
    fun getCustomizedDialogTitle(titleText: String, context: Context) : View {
        //CUSTOM TITLE
        val title = TextView(context)
        title.text = titleText
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        title.gravity = Gravity.CENTER
        title.setTextColor(ContextCompat.getColor(context, R.color.black))
        title.background = AppCompatResources.getDrawable(context, R.drawable.custom_dialog_title_bg)
        title.setPadding(0, 30, 0, 30)

        return title
    }
}
