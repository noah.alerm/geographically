package com.itb.geographically.utils

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.view.setPadding
import com.itb.geographically.data.models.MarkerGeo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream

object CameraAndGalleryManagement {
    //METHODS
    /**
     * This method is used to manage the camera and gallery requests.
     * @param requestCode The activity's request code
     * @param resultCode The activity's result code (RESULT_OK if successful)
     * @param data The Intent used to do the request
     * @param activity Activity used to create a content resolver and to access resources
     */
    private fun manageRequests(requestCode: Int, resultCode: Int, data: Intent?, activity: Activity) : Bitmap? {
        val imageBitmap: Any?
        var bitmap: Bitmap? = null

        //CAMERA
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            //IMAGE FROM CAMERA
            imageBitmap = data!!.extras!!.get("data")
            bitmap = Bitmap.createScaledBitmap(imageBitmap as Bitmap, 500, 500, false)

        }
        //GALLERY
        else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            //IMAGE FROM GALLERY
            val imageUri = data!!.data
            imageBitmap = MediaStore.Images.Media.getBitmap(activity.contentResolver, imageUri)
            bitmap = Bitmap.createScaledBitmap(imageBitmap as Bitmap, 500, 500, false)
        }

        return bitmap
    }

    /**
     * This method is used to give a format to the bitmap obtained by the camera or the gallery.
     * @param requestCode The activity's request code
     * @param resultCode The activity's result code (RESULT_OK if successful)
     * @param data The Intent used to do the request
     * @param activity Activity used to create a content resolver and to access resources
     * @param image ImageView where the bitmap is inserted
     */
    fun getBitmapFromRequest(requestCode: Int, resultCode: Int, data: Intent?, activity: Activity, image: ImageView, isProfileIcon: Boolean) : Bitmap? {
        val bitmap: Bitmap? = manageRequests(requestCode, resultCode, data, activity)

        //BITMAP SET UP
        if (bitmap != null) {
            val img = RoundedBitmapDrawableFactory.create(activity.resources, bitmap)
            if (isProfileIcon) {
                img.cornerRadius = 250F
                image.setPadding(50)
            }
            else
                img.cornerRadius = 30F
            image.setImageDrawable(img)
        }

        return bitmap
    }

    /**
     * This method is used to insert an image into the Firebase storage.
     * @param marker Marker that contains the image
     */
    fun addImageToStorage(marker: MarkerGeo) {
        //BITMAP COMPRESSION
        val baos = ByteArrayOutputStream()
        marker.image!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        //FIREBASE STORAGE INSERTION
        FirebaseStorage.getInstance().reference.child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/" + marker.id)
            .putBytes(data)
    }

    /**
     * This method is used to insert the profile image into the Firebase storage.
     * @param bitmap Bitmap
     */
    fun addProfileImageToStorage(bitmap: Bitmap?) {
        //BITMAP COMPRESSION
        val baos = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        //FIREBASE STORAGE INSERTION
        FirebaseStorage.getInstance().reference.child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/profileIcon")
            .putBytes(data)
    }
}
