package com.itb.geographically

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.itb.geographically.R
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage

class MainActivity : AppCompatActivity() {
    //ATTRIBUTES
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var userIcon: ImageView
    private lateinit var username: TextView
    private lateinit var email: TextView
    private lateinit var logoutText: TextView
    private lateinit var logoutIcon: ImageView

    //ON CREATE
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //IDs
        drawerLayout = findViewById(R.id.drawerLayout)
        navigationView = findViewById(R.id.navigationView)
        val headerView = navigationView.getHeaderView(0)
        username = headerView.findViewById(R.id.username)
        email = headerView.findViewById(R.id.email)
        userIcon = headerView.findViewById(R.id.user_icon)
        logoutText = findViewById(R.id.logout_text)
        logoutIcon = findViewById(R.id.logout_icon)

        //DRAWER SWIPE DISABLED
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        //DRAWER LISTENER
        drawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener{
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            //ON DRAWER OPENED (Used when the user changes their account in order to update the following data when the drawer is opened)
            override fun onDrawerOpened(drawerView: View) {
                setUpUserData()
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
            }
        })

        setUpUserData()

        //ON CLICK
        //User Icon
        userIcon.setOnClickListener {
            drawerLayout.close()

            //NAVIGATION
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.userFragment)
        }

        //Logout Text
        logoutText.setOnClickListener {
            goToLoginScreen()
        }

        //Logout Icon
        logoutIcon.setOnClickListener {
            goToLoginScreen()
        }
    }

    //METHODS
    /**
     * This method is used to navigate to the login screen.
     */
    private fun goToLoginScreen() {
        drawerLayout.close()

        //SIGN OUT
        FirebaseAuth.getInstance().signOut()

        //NAVIGATION
        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.loginFragment)
    }

    /**
     * This method is used to set up the user's data.
     */
    @SuppressLint("SetTextI18n")
    private fun setUpUserData() {
        //USER DATA SET UP
        if (FirebaseAuth.getInstance().currentUser != null) {
            var imageUri: Uri? = null

            FirebaseStorage.getInstance().getReferenceFromUrl("gs://geographically-341418.appspot.com/")
                .child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/profileIcon")
                .downloadUrl.addOnSuccessListener {
                    imageUri = it
                    Log.d("URI", imageUri.toString())
                }

            Handler(Looper.getMainLooper()).postDelayed({
                if (imageUri != null) {
                    Glide.with(this).asBitmap().load(imageUri).into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            val img = RoundedBitmapDrawableFactory.create(resources, resource)
                            img.cornerRadius = 250F
                            userIcon.setImageDrawable(img)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                    })
                }
                else
                    userIcon.setImageResource(TypedValue().also { theme.resolveAttribute(R.attr.user_icon, it, true) }.resourceId)
            }, 1000)
        }

        username.text = FirebaseAuth.getInstance().currentUser?.displayName.toString()
        if (FirebaseAuth.getInstance().currentUser?.displayName == null
            || FirebaseAuth.getInstance().currentUser?.displayName == "")
            username.text = "Username"
        email.text = FirebaseAuth.getInstance().currentUser?.email
    }
}
